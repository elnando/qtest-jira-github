#
# Pre-requisites (should exists in gitlab as variables)
#

if [ -z "${QTEST_ENABLED}" ] 
then

	if [ -z "${PROJECTID}" ] | [ -z ${GITLAB_PRIVATE_TOKEN} ]
	then
		echo "GITLAB_PRIVATE_TOKEN or PROJECTID variables should be defined in the gitlab-ci settings"  >&2
		exit 2
	fi

	CURL=/usr/bin/curl
	BASEURI=https://ibment.qtestnet.com/api/v3/projects/${PROJECTID}
	INPUT_TESTS=tests.input

	#
	# Check if Parent module already exists and is only one
	#

	echo result-logger-cli create module --name ${CI_PROJECT_TITLE} --description "Main modules required for qtests" module_creation.json
	PARENTID=9227427

	#
	# Create baseline modules, using \047 (') and \042 (") to avoid collitions
	#

	GITLAB_API_URL="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/repository/commits"
	QTEST_VARS_FILE=qtest_vars.yml

	cat ${INPUT_TESTS}
	ACTIONS=$(awk -F: ' { print "   {\042action\042: \042create\042, \042file_path\042: \042"$4"/.desc\042,\042content\042: \042"$2"\042}," } ' ${INPUT_TESTS} )

	if [ ! -f ${QTEST_VARS_FILE} ]
	then
		echo "variables:" > ${QTEST_VARS_FILE}
		echo "  QTEST_ENABLED: \"yes\"" >> ${QTEST_VARS_FILE}
		IFST=$IFS
		IFS=:
		while read  MODULE_NAME MODULE_DESCRIPTION MODULE_VARIABLE MODULE_PATH
		do
		  echo result-logger-cli create module --parent $PARENTID --name $MODULE_NAME --description $MODULE_DESCRIPTION module_creation.json
		  PID=$( jq '.pid' module_creation.json  )
		  echo "  $MODULE_VARIABLE: $PID" >> ${QTEST_VARS_FILE}


		done < ${INPUT_TESTS}
		IFS=$IFST
		sed  -e "1iinclude: '${QTEST_VARS_FILE}'" -i  .gitlab-ci.yml
		GITLAB_CI_CONTENT="$( sed -e ':a;N;$!ba;s/\n/\\n/g'  -e 's/"/\\"/g'  .gitlab-ci.yml)"
		LAST_ACTION=$(cat << JSON

	{
      "action": "update",
      "file_path": ".gitlab-ci.yml",
      "content": "${GITLAB_CI_CONTENT}"
	},
	{
      "action": "create",
      "file_path": "$QTEST_VARS_FILE",
      "content": "$(sed -e ':a;N;$!ba;s/\n/\\n/g' -e 's/"/\\"/g' ${QTEST_VARS_FILE})"
	}
JSON
)

		PAYLOAD=$(cat << JSON
{
  "branch": "master",
  "commit_message": "some commit message",
  "actions": [ $ACTIONS $LAST_ACTION ]
}
JSON
)

		echo "$PAYLOAD" > payloadfile.json
		curl -v  --request POST --header "PRIVATE-TOKEN: ${GITLAB_PRIVATE_TOKEN}" --header "Content-Type: application/json" --data @payloadfile.json  $GITLAB_API_URL 
	fi
fi

